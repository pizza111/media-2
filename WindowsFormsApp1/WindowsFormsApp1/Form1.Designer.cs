﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Start = new System.Windows.Forms.Button();
            this.textBoxAdres = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelRSA = new System.Windows.Forms.Label();
            this.buttonRSA = new System.Windows.Forms.Button();
            this.textBoxValue = new System.Windows.Forms.TextBox();
            this.textBoxSzyfr = new System.Windows.Forms.TextBox();
            this.textBoxDeszyfr = new System.Windows.Forms.TextBox();
            this.encryptedFileName = new System.Windows.Forms.TextBox();
            this.encrypt_button = new System.Windows.Forms.Button();
            this.decryptedFileName = new System.Windows.Forms.TextBox();
            this.decrypt_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea6.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.chart1.Legends.Add(legend6);
            this.chart1.Location = new System.Drawing.Point(338, 229);
            this.chart1.Name = "chart1";
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "FFT";
            this.chart1.Series.Add(series6);
            this.chart1.Size = new System.Drawing.Size(651, 259);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // Start
            // 
            this.Start.Location = new System.Drawing.Point(338, 57);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(131, 34);
            this.Start.TabIndex = 1;
            this.Start.Text = "Start";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click_1);
            // 
            // textBoxAdres
            // 
            this.textBoxAdres.Location = new System.Drawing.Point(338, 29);
            this.textBoxAdres.Name = "textBoxAdres";
            this.textBoxAdres.Size = new System.Drawing.Size(426, 22);
            this.textBoxAdres.TabIndex = 2;
            this.textBoxAdres.Text = "C:\\Users\\fiwo1\\Desktop\\JetEngine.wav";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(335, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Adres do pliku";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Info";
            // 
            // labelRSA
            // 
            this.labelRSA.AutoSize = true;
            this.labelRSA.Location = new System.Drawing.Point(936, 23);
            this.labelRSA.Name = "labelRSA";
            this.labelRSA.Size = new System.Drawing.Size(36, 17);
            this.labelRSA.TabIndex = 6;
            this.labelRSA.Text = "RSA";
            // 
            // buttonRSA
            // 
            this.buttonRSA.Location = new System.Drawing.Point(1070, 54);
            this.buttonRSA.Name = "buttonRSA";
            this.buttonRSA.Size = new System.Drawing.Size(75, 23);
            this.buttonRSA.TabIndex = 7;
            this.buttonRSA.Text = "RSA";
            this.buttonRSA.UseVisualStyleBackColor = true;
            this.buttonRSA.Click += new System.EventHandler(this.buttonRSA_Click);
            // 
            // textBoxValue
            // 
            this.textBoxValue.Location = new System.Drawing.Point(1070, 23);
            this.textBoxValue.Name = "textBoxValue";
            this.textBoxValue.Size = new System.Drawing.Size(100, 22);
            this.textBoxValue.TabIndex = 8;
            this.textBoxValue.Text = "1";
            // 
            // textBoxSzyfr
            // 
            this.textBoxSzyfr.Location = new System.Drawing.Point(1070, 87);
            this.textBoxSzyfr.Name = "textBoxSzyfr";
            this.textBoxSzyfr.Size = new System.Drawing.Size(100, 22);
            this.textBoxSzyfr.TabIndex = 9;
            this.textBoxSzyfr.Text = "Zaszyfrowana";
            // 
            // textBoxDeszyfr
            // 
            this.textBoxDeszyfr.Location = new System.Drawing.Point(1070, 130);
            this.textBoxDeszyfr.Name = "textBoxDeszyfr";
            this.textBoxDeszyfr.Size = new System.Drawing.Size(100, 22);
            this.textBoxDeszyfr.TabIndex = 10;
            this.textBoxDeszyfr.Text = "Odszyfrowana";
            // 
            // encryptedFileName
            // 
            this.encryptedFileName.Location = new System.Drawing.Point(338, 97);
            this.encryptedFileName.Name = "encryptedFileName";
            this.encryptedFileName.Size = new System.Drawing.Size(426, 22);
            this.encryptedFileName.TabIndex = 11;
            this.encryptedFileName.Text = "lambada.wav";
            this.encryptedFileName.Visible = false;
            // 
            // encrypt_button
            // 
            this.encrypt_button.Location = new System.Drawing.Point(338, 125);
            this.encrypt_button.Name = "encrypt_button";
            this.encrypt_button.Size = new System.Drawing.Size(131, 34);
            this.encrypt_button.TabIndex = 12;
            this.encrypt_button.Text = "Szyfruj";
            this.encrypt_button.UseVisualStyleBackColor = true;
            this.encrypt_button.Visible = false;
            this.encrypt_button.Click += new System.EventHandler(this.encrypt_button_Click);
            // 
            // decryptedFileName
            // 
            this.decryptedFileName.Location = new System.Drawing.Point(338, 165);
            this.decryptedFileName.Name = "decryptedFileName";
            this.decryptedFileName.Size = new System.Drawing.Size(426, 22);
            this.decryptedFileName.TabIndex = 13;
            this.decryptedFileName.Text = "delambada.wav";
            this.decryptedFileName.Visible = false;
            // 
            // decrypt_button
            // 
            this.decrypt_button.Location = new System.Drawing.Point(338, 189);
            this.decrypt_button.Name = "decrypt_button";
            this.decrypt_button.Size = new System.Drawing.Size(131, 34);
            this.decrypt_button.TabIndex = 14;
            this.decrypt_button.Text = "Deszyfruj";
            this.decrypt_button.UseVisualStyleBackColor = true;
            this.decrypt_button.Visible = false;
            this.decrypt_button.Click += new System.EventHandler(this.decrypt_button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1208, 500);
            this.Controls.Add(this.decrypt_button);
            this.Controls.Add(this.decryptedFileName);
            this.Controls.Add(this.encrypt_button);
            this.Controls.Add(this.encryptedFileName);
            this.Controls.Add(this.textBoxDeszyfr);
            this.Controls.Add(this.textBoxSzyfr);
            this.Controls.Add(this.textBoxValue);
            this.Controls.Add(this.buttonRSA);
            this.Controls.Add(this.labelRSA);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxAdres);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.chart1);
            this.Name = "Form1";
            this.Text = "E-media";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.TextBox textBoxAdres;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelRSA;
        private System.Windows.Forms.Button buttonRSA;
        private System.Windows.Forms.TextBox textBoxValue;
        private System.Windows.Forms.TextBox textBoxSzyfr;
        private System.Windows.Forms.TextBox textBoxDeszyfr;
        private System.Windows.Forms.TextBox encryptedFileName;
        private System.Windows.Forms.Button encrypt_button;
        private System.Windows.Forms.TextBox decryptedFileName;
        private System.Windows.Forms.Button decrypt_button;
    }
}

