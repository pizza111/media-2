﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace WindowsFormsApp1
{
    class RSA
    {
        public int E;
        public int N;
        public int D;

        private int NWD(int a, int b)
        {
            int x ;
            while( b != 0)
            {
                x = b;
                b = a % b;
                a = x;
            }
            return a;
        }

        private int InverseModulo(int e, int fi)
        {
            int u, w, x, z;
            int q;
            int pom;
            u = 1;
            w = e;
            x = 0;
            z = fi;

            while(w != 0)
            {
                if ( w < z)
                {
                    pom = u;
                    u = x;
                    x = pom;
                    pom = w;
                    w = z;
                    z = pom;
                }
                q = w / z;
                u = u - (q * x);
                w = w - (q * z);
            }
            if (z != 1) MessageBox.Show("źle dobrano E");
            if(x < 0)
            {
                x = x + fi;
            }
            return x;
        }
        private void CreateKey()
        {
            int p = 101;
            int q = 197;

            int fi = (p - 1) * (q - 1);
            N = p * q;

            int t = fi;
            int e = 3;

            while(t != 1)
            {
                e += 2;
                t = NWD(e, fi);
            }
            E = e;
            D = InverseModulo(e, fi);
        }
        private int PotegaZModulo(int podstawa, int potega, int modulo)
        {
            int pot = podstawa, wynik = 1;
            for (int i = potega; i > 0; i /= 2)
            {
                if (i % 2 == 1) wynik = (wynik * pot) % N;
                pot = (pot * pot) % N;
            }
            return wynik;
        }
        public void Encryption(ref int m)
        {
            m = PotegaZModulo(m,E,N);
        }
        public void Decryption(ref int m)
        {
            m = PotegaZModulo(m,D,N);
        }
        public RSA()
        {
            CreateKey();
        }
        public String RSAToString()
        {
            String desc = "RSA: \n";
            desc += "N =" + N + "\n";
            desc += "E =" + E + "\n";
            desc += "D =" + D + "\n";

            return desc;
        }
    }
}
