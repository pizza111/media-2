﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        WAVFile handeledWAV;
        RSA rsa;

        public Form1()
        {
            InitializeComponent();
        }


        private void Start_Click_1(object sender, EventArgs e)
        {
            handeledWAV = new WAVFile(textBoxAdres.Text);
            label2.Text = handeledWAV.StringData();
            Transformata wsk = new Transformata();
            wsk.Ftt(handeledWAV.SamplesL, 1024, handeledWAV.SampleRate, 1);
            var tmp = new LomontFFT();
            int[] tab2 = new int[1024];
            double[] tab = new double[1024];
            rsa = new RSA();

            encryptedFileName.Visible = true;
            encrypt_button.Visible = true;
            decryptedFileName.Visible = true;
            decrypt_button.Visible = true;

            for(int i = 0; i < 1024; ++i)
                tab2[i] = handeledWAV.SamplesL[i];

            // szyfrowanie
            for (int i = 0; i < 1024; ++i)
                rsa.Encryption(ref tab2[i]);

            //// deszyfrowanie
            for (int i = 0; i < 1024; ++i)
            {
                rsa.Decryption(ref tab2[i]); 
            }

            // do FTT potrzebny jest typ double
            for (int i = 0; i < 1024; ++i)
                tab[i] = (double)tab2[i];
            tmp.RealFFT(tab, true);

            for(int i = 0; i < 1024; ++i)
            {
                // chart1.Series["FFT"].Points.AddXY("" , wsk.vector[i]);
                chart1.Series["FFT"].Points.AddXY("", tab[i]);
            }
 

        }

        private void buttonRSA_Click(object sender, EventArgs e)
        {
            RSA rsa = new RSA();
            labelRSA.Text = rsa.RSAToString();
            int m = Int32.Parse( textBoxValue.Text);
            rsa.Encryption(ref m);
            textBoxSzyfr.Text = "Szyfr = " + m;
            rsa.Decryption(ref m);
            textBoxDeszyfr.Text = "DeSzyfr = " + m;
        }

        private void encrypt_button_Click(object sender, EventArgs e)
        {
            handeledWAV.EncryptToFile(encryptedFileName.Text, ref rsa);
        }

        private void decrypt_button_Click(object sender, EventArgs e)
        {
            handeledWAV.DecryptToFile(decryptedFileName.Text, ref rsa);
        }
    }
}
