﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WindowsFormsApp1
{
    class WAVFile
    {
        private string _fileName;
        public string ChunkID;
        public int ChunkSize;
        public string Format;
        public string Subchunk1ID;
        public int Subchunk1Size;
        public int AudioFormat;
        public int NumChannels;
        public int SampleRate;
        public int ByteRate;
        public int BlockAlign;
        public int BitsPerSample;

        public int ExtraParamSize;
        public String ExtraParams;

        public String Subchunk2ID;
        public int Subchunk2Size;
        public int NumOfSamples;

        public int[] SamplesL;
        public int[] SamplesP;

        public WAVFile(string fileName)
        {
            _fileName = fileName;
            ReadDataFromFile();
        }

        private void ReadDataFromFile()
        {
            int tmpOff;

            byte[] bytes = System.IO.File.ReadAllBytes(_fileName);

            ASCIIEncoding ascii = new ASCIIEncoding();

            ChunkID = ascii.GetString(bytes, 0, 4);
            //ChunkSize = ascii.GetString(bytes, 4, 4);
            ChunkSize = BitConverter.ToInt32(bytes, 4);
            Format = ascii.GetString(bytes, 8, 4);

            Subchunk1ID = ascii.GetString(bytes, 12, 4);
            Subchunk1Size = BitConverter.ToInt32(bytes, 16);
            AudioFormat = BitConverter.ToInt16(bytes, 20);
            NumChannels = BitConverter.ToInt16(bytes, 22);

            SampleRate = BitConverter.ToInt32(bytes, 24);
            ByteRate = BitConverter.ToInt32(bytes, 28);
            BlockAlign = BitConverter.ToInt16(bytes, 32);
            BitsPerSample = BitConverter.ToInt16(bytes, 34);
            tmpOff = 36;

            if (Subchunk1Size != 16)
            {
                ExtraParamSize = BitConverter.ToInt16(bytes, 36);
                ExtraParams = ascii.GetString(bytes, 38, ExtraParamSize);
                tmpOff = 36 + ExtraParamSize;
            }

            Subchunk2ID = ascii.GetString(bytes, tmpOff, 4);
            Subchunk2Size = BitConverter.ToInt32(bytes, (tmpOff += 4));

            NumOfSamples = (Subchunk2Size / NumChannels) / (BitsPerSample / 8);
            if (NumChannels == 2) SamplesP = new int[NumOfSamples];
            SamplesL = new int[NumOfSamples];

            switch (BitsPerSample)
            {
                case 16:
                    for (int i = 0; i < NumOfSamples; i++) for (int j = 0; j < NumChannels; j++)
                    {
                            if (j == 0) SamplesL[i] = BitConverter.ToInt16(bytes, (tmpOff += 2));
                            else if (j == 1) SamplesP[i] = BitConverter.ToInt16(bytes, (tmpOff += 2));
                    }
                    break;
                case 32:
                    for (int i = 0; i < NumOfSamples; i++) for (int j = 0; j < NumChannels; j++)
                    {
                            if (j == 0) SamplesL[i] = BitConverter.ToInt32(bytes, (tmpOff += 4));
                            else if (j == 1) SamplesP[i] = BitConverter.ToInt32(bytes, (tmpOff += 4));
                    }
                    break;
                    //case 64: for (int i = 0; i < NumOfSamples; i++) for (int j = 0; j < NumChannels; j++) Samples[i, j] = BitConverter.ToInt64(bytes, (tmpOff += 8)); break;
            }

        }

        public String StringData()
        {
            String value = "";
            value += "ChunkID " + ChunkID + "\n";
            value += "ChunkSize " + ChunkSize + "\n";
            value += "Format " + Format + "\n\n";

            value += "Subchunk1ID " + Subchunk1ID + "\n";
            value += "Subchunk1Size " + Subchunk1Size + "\n";
            value += "AudioFormat " + AudioFormat + "\n";
            value += "NumChannels " + NumChannels + "\n";
            value += "SampleRate " + SampleRate + "\n";
            value += "ByteRate " + ByteRate + "\n";
            value += "BlockAlgin " + BlockAlign + "\n";
            value += "BitsPerSample " + BitsPerSample + "\n\n";

            if (Subchunk1Size != 16)
            {
                value += "ExtraParamSize " + ExtraParamSize + "\n";
                value += "ExtraParams" + ExtraParams + "\n\n";
            }

            value += "Subchunk2ID " + Subchunk2ID + "\n";
            value += "Subchunk2Size " + Subchunk2Size + "\n";
            value += "NumOfSamples " + NumOfSamples + "\n\n";

            return value;
        }

        public void EncryptToFile(string fileName, ref RSA rsa)
        {
            /************************************************************************/
            int[] newSamplesL;
            int[] newSamplesP;
            if (NumChannels == 1)
            {
                newSamplesL = new int[NumOfSamples];
                newSamplesP = new int[1];
            }
            else
            {
                newSamplesL = new int[NumOfSamples];
                newSamplesP = new int[NumOfSamples];
            }

            for (int i = 0; i < NumOfSamples; i++) for (int j = 0; j < NumChannels; j++)
                {
                    if (j == 0) newSamplesL[i] = SamplesL[i];
                    else if (j == 1) newSamplesP[i] = SamplesP[i]; ;
                }

            for (int i = 0; i < NumOfSamples; i++) for (int j = 0; j < NumChannels; j++)
            {
                    if (j == 0) rsa.Encryption(ref newSamplesL[i]);
                    else if (j == 1) rsa.Encryption(ref newSamplesP[i]);
            }
            /*******************************************************************************/


            FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
            
            fileStream.Write(Encoding.ASCII.GetBytes(ChunkID), 0, 4);
            //ChunkSize = ascii.GetString(bytes, 4, 4);
            fileStream.Write(BitConverter.GetBytes(ChunkSize), 0, 4);
            fileStream.Write(Encoding.ASCII.GetBytes(Format), 0, Encoding.ASCII.GetBytes(Format).Length);

            fileStream.Write(Encoding.ASCII.GetBytes(Subchunk1ID), 0, Encoding.ASCII.GetBytes(Subchunk1ID).Length);
            fileStream.Write(BitConverter.GetBytes(Subchunk1Size), 0, 4);
            fileStream.Write(BitConverter.GetBytes(AudioFormat), 0, 2);
            fileStream.Write(BitConverter.GetBytes(NumChannels), 0, 2);

            fileStream.Write(BitConverter.GetBytes(SampleRate), 0, 4);
            fileStream.Write(BitConverter.GetBytes(ByteRate), 0, 4);
            fileStream.Write(BitConverter.GetBytes(BlockAlign), 0, 2);
            fileStream.Write(BitConverter.GetBytes(BitsPerSample), 0, 2);
            
            if (Subchunk1Size != 16)
            {
                fileStream.Write(BitConverter.GetBytes(ExtraParamSize), 0, 2);
                fileStream.Write(Encoding.ASCII.GetBytes(ExtraParams), 0, Encoding.ASCII.GetBytes(ExtraParams).Length);
            }

            fileStream.Write(Encoding.ASCII.GetBytes(Subchunk2ID), 0, Encoding.ASCII.GetBytes(Subchunk2ID).Length);
            fileStream.Write(BitConverter.GetBytes(Subchunk2Size), 0, 4);


            for (int i = 0; i < NumOfSamples; i++) for (int j = 0; j < NumChannels; j++)
            {
                if (j == 0) fileStream.Write(BitConverter.GetBytes(newSamplesL[i]), 0, (BitsPerSample == 16)? 2 : 4);
                else if (j == 1) fileStream.Write(BitConverter.GetBytes(newSamplesP[i]), 0, (BitsPerSample == 16) ? 2 : 4);
            }

            fileStream.Close();
        }

        public void DecryptToFile(string fileName, ref RSA rsa)
        {
            /************************************************************************/
            int[] newSamplesL;
            int[] newSamplesP;
            if (NumChannels == 1)
            {
                newSamplesL = new int[NumOfSamples];
                newSamplesP = new int[1];
            }
            else
            {
                newSamplesL = new int[NumOfSamples];
                newSamplesP = new int[NumOfSamples];
            }

            for (int i = 0; i < NumOfSamples; i++) for (int j = 0; j < NumChannels; j++)
            {
                if (j == 0) newSamplesL[i] = SamplesL[i];
                else if (j == 1) newSamplesP[i] = SamplesP[i]; ;
            }

            for (int i = 0; i < NumOfSamples; i++) for (int j = 0; j < NumChannels; j++)
            {
                if (j == 0) rsa.Decryption(ref newSamplesL[i]);
                else if (j == 1) rsa.Decryption(ref newSamplesP[i]);
            }
            /*******************************************************************************/


            FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);

            fileStream.Write(Encoding.ASCII.GetBytes(ChunkID), 0, 4);
            //ChunkSize = ascii.GetString(bytes, 4, 4);
            fileStream.Write(BitConverter.GetBytes(ChunkSize), 0, 4);
            fileStream.Write(Encoding.ASCII.GetBytes(Format), 0, Encoding.ASCII.GetBytes(Format).Length);

            fileStream.Write(Encoding.ASCII.GetBytes(Subchunk1ID), 0, Encoding.ASCII.GetBytes(Subchunk1ID).Length);
            fileStream.Write(BitConverter.GetBytes(Subchunk1Size), 0, 4);
            fileStream.Write(BitConverter.GetBytes(AudioFormat), 0, 2);
            fileStream.Write(BitConverter.GetBytes(NumChannels), 0, 2);

            fileStream.Write(BitConverter.GetBytes(SampleRate), 0, 4);
            fileStream.Write(BitConverter.GetBytes(ByteRate), 0, 4);
            fileStream.Write(BitConverter.GetBytes(BlockAlign), 0, 2);
            fileStream.Write(BitConverter.GetBytes(BitsPerSample), 0, 2);

            if (Subchunk1Size != 16)
            {
                fileStream.Write(BitConverter.GetBytes(ExtraParamSize), 0, 2);
                fileStream.Write(Encoding.ASCII.GetBytes(ExtraParams), 0, Encoding.ASCII.GetBytes(ExtraParams).Length);
            }

            fileStream.Write(Encoding.ASCII.GetBytes(Subchunk2ID), 0, Encoding.ASCII.GetBytes(Subchunk2ID).Length);
            fileStream.Write(BitConverter.GetBytes(Subchunk2Size), 0, 4);


            for (int i = 0; i < NumOfSamples; i++) for (int j = 0; j < NumChannels; j++)
                {
                    if (j == 0) fileStream.Write(BitConverter.GetBytes(newSamplesL[i]), 0, (BitsPerSample == 16) ? 2 : 4);
                    else if (j == 1) fileStream.Write(BitConverter.GetBytes(newSamplesP[i]), 0, (BitsPerSample == 16) ? 2 : 4);
                }

            fileStream.Close();
        }
    }
}
